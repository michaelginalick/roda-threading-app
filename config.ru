require "roda"
require "rest-client"

class App < Roda
  route do |r|
    Thread.abort_on_exception = true
		r.is "cpu" do
			threads = []
			threads << Thread.new { 2*2 }
			threads << Thread.new { 10*10 }
			threads.map { |thr|  puts thr.status }
			results = threads.map { |thr| thr.join(25) }.map {|thr| thr.value }

			results.to_s
    end
    
    r.is "file_IO" do
      thread = []

      thread << Thread.new { out = File.new("out.txt", "w")
                  out.close
                }

      thread.map {|thr| puts thr.status }
      results = thread.map { |thr| thr.join(25) }.map{ |thr| thr.value }

      results.to_s
    end

    r.is "http_IO" do
      thread = []

      thread << Thread.new { RestClient.get("https://jsonplaceholder.typicode.com/posts") }
      thread.map {|thr| puts thr.status }
      results = thread.map { |thr| thr.join(25) }.map{ |thr| thr.value }
      puts results
      results.to_s
    end

    r.is "sleep" do
      thread = []
      thread << Thread.new { sleep(10) }
      thread.map {|thr| puts thr.status }

      results = thread.map { |thr| thr.join(25) }.map{ |thr| thr.value }
      results.to_s
    end

    r.is "exception" do
      t1 = Thread.new do
        puts "I am here for exception"
        raise "Exception from thread"
      end
      t1.to_s
    end

	end
end

run App
