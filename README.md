# roda-threading-app

This is an example of threading in a roda endpoint, and the issues associated with performing IO.



Installation instructions

1) Git clone
2) bundle install
3) bundle exec rackup
4) navigate to http://localhost:9292/#{endpoint}


Individual endpoint behavior

`cpu`

This is expected behavior

Logs: run, false
Web: [4, 100]

--------------------------------------------------
`file_IO`

Logs: run

Web: [nil]

Creates and writes to file which is expected behavior

----------------------------------------------------

`http_IO`

Logs: run

Web: []

The expected behavior of this route would be to make the http request, and block on join for 25 seconds. If the thread does not complete within that time return. 

The actual behavior is the route returns immediately and does not wait for the thread to complete.

----------------------------------------------------

`sleep`

Logs: NA

Web: [10]

The expected behavior is the endpoint sleeps for 10 seconds before continuing.

The actual behavior is the endpoint sleeps for 10 seconds before continuing

-----------------------------------------------------

`exception`

Logs: I am here for exception

Web: RuntimeError at /exception

The expected behavior is the endpoint prints message to console before raising exception.

The actual behavior is the endpoint prints message to console and raises exception.

